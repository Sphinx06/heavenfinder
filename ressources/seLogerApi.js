const parser = require('fast-xml-parser')
const he = require('he')
const fetch = require("node-fetch")

export default class API {
    // INIT XML PARSER
    constructor () {
        this.options = {
            attributeNamePrefix: "@_",
            attrNodeName: "attr", //default is 'false'
            textNodeName: "#text",
            ignoreAttributes: true,
            ignoreNameSpace: false,
            allowBooleanAttributes: false,
            parseNodeValue: true,
            parseAttributeValue: false,
            trimValues: true,
            cdataTagName: "__cdata", //default is 'false'
            cdataPositionChar: "\\c",
            localeRange: "", //To support non english character in tag/attribute values.
            parseTrueNumberOnly: false,
            attrValueProcessor: a => he.decode(a, { isAttributeValue: true }),//default is a=>a
            tagValueProcessor: a => he.decode(a) //default is a=>a
        }
    }

    // Fonction API SELOGER.COM
    async seLogerSearch (typebien, pxmin, pxmax, idtt, naturebien, cp) {

        // Déclaration des variables / constantes
        const baseSearchUrl = "http://ws.seloger.com/search.xml?"
        const optionsSearch = [
            typeGood = "idtypebien=" + typebien.join(","),
            prixMax = "pxMax=" + pxmax,
            prixMini = "pMin=" + pxmin,
            rentOrBuy = "idtt=" + idtt,
            natureGood = "naturebien=" + naturebien.join(","),
            postalCode = "cp=" + cp
        ]
        const query = baseSearchUrl + optionsSearch.join("&")

        // Récupération des données de recherches
        let result = await seLogerSearchAux(query)

        return result
    }

    // Fonction de recherche récurAPI 
    async seLogerSearchAux (query) {
        console.log(query)
        try {
            // Requête API
            const response = await fetch(query)
            // Vérification 
            if (!response.ok)
                throw new Error(response.statusText)
            else {
                // Récupération du texte du body 
                const xmlData = await response.text()
                // Vérification si possible de parse XML
                if (parser.validate(xmlData) === true) {
                    // Parsing XML
                    const info = await parser.parse(xmlData, this.options)
                    console.log("Page courante:" + info.recherche.pageCourante)
                    // Vérificaiton si existant et si page suivante existante 
                    if (info) {
                        if (info.recherche.pageSuivante && info.recherche.pageSuivante != "") {
                            // Récupération des annonces des autres page par récursivité
                            let next = await seLogerSearchAux(info.recherche.pageSuivante)
                            if (next.recherche.annonces) {
                                if (next.recherche.annonces.annonce)
                                    info.recherche.annonces.annonce.push(next.recherche.annonces.annonce)

                            }
                        }
                        console.log("Nombre d'annonces" + Object.keys(info.recherche.annonces.annonce).length)
                        return info
                    }
                }
            }
            throw new Error("Aucune donnée à parser")
        } catch (err) {
            console.log(err)
            return null
        }
    }
}