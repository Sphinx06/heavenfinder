const fetch = require("node-fetch")
const fs = require('fs');



function addZero (i) {
    if (i < 10) {
        i = "0" + i;
    }
    return i;
}

function getDateTimeNowString () {
    var d = new Date();
    var day = addZero(d.getDay());
    var month = addZero(d.getMonth())
    var year = addZero(d.getFullYear())
    var h = addZero(d.getHours());
    var m = addZero(d.getMinutes());
    var s = addZero(d.getSeconds());
    return day + "_" + month + "_" + year + "-" + h + "_" + m + "_" + s;
}

// Fonction API SELOGER.COM
async function searching (showResult, saveResult) {

    // Déclaration des variables / constantes
    const baseSearchUrl = "https://immo.cooya.fr/query"


    let typebien = ["rentals", "sales"];
    let surfaceMin = "";
    let surfaceMax = "";
    let sortBy = "date-desc"
    let roomsMin = ""
    let roomsMax = ""
    let priceMin = ""
    let priceMax = ""
    let page = 3
    let categories = ["flat", "house"]
    let locations = [
        { city: "Paris", postalCode: "75000" },
        { city: "Évry", postalCode: "91000" }
    ]

    let post = {
        types: typebien,
        categories: categories,
        locations: locations,
        surfaceMin: surfaceMin,
        sortBy: sortBy,
        roomsMin: roomsMin,
        roomsMax: roomsMax,
        priceMin: priceMin,
        priceMax: priceMax,
        page: page
    };

    console.log(JSON.stringify(post))
    // Récupération des données de recherches
    fetch(baseSearchUrl, {
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json; charset=utf-8'
        },
        method: "POST",
        body: JSON.stringify(post)
    }).then(function (response) {
        return response.json();
    }).then(function (data) {
        if (showResult) {
            console.log(data)
        }
        if (saveResult) {
            fs.writeFile('resultScrapper-' + getDateTimeNowString() + '.json', JSON.stringify(data), err => {
                if (err) {
                    console.log('Error writing file', err)
                } else {
                    console.log('Successfully wrote file')
                }
            })
        }
    })

}

searching(true, true);
