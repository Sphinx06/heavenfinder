
function test () {
    let se = new SearchEngine()
    
}

class SearchEngine {

    constructor () {
        this.docs = []
        this.query = null
    }

    sum (arr) {
        return arr.reduce(function (acc, x) { return acc + x }, 0)
    }

    makeTokens (text) {
        if (text === null) { return [] }
        if (text.length === 0) { return [] }
        return text.toLowerCase().replace(
            /[~`’!@#$%^&*(){}\[\];:"'<,.>?\/\\|_+=-]/g
            , ''
        ).split(' ').filter(function (token) { return token.length > 0 })
    };

    makeTfVector (countVector) {
        let total = sum(countVector)
        return countVector.map(
            function (count) {
                return total === 0 ? 0 : count / total
            }
        )
    };

    addDoc () {
        this.docs.push({ text: "", id: Date.now() })
        this.$nextTick(
            this.docsNavSetup
        )
    }
    removeDoc (button) {
        let id = parseInt(button.target.attributes.docId.value, 10)
        this.docs = this.docs.filter(
            function (doc) {
                return (doc.id !== id)
            }
        )
    }
    rankScoredDocs (scores) {
        return scores.map(
            function (score, index) {
                let doc = this.docs[index]
                doc.index = index
                return [score, doc]
            }.bind(this)
        ).sort(function (a, b) { return -a[0] + b[0] }).map(
            function (elem) {
                return elem[1]
            }
        )
    }
    docsNavSetup () {
        this.docs.map(
            function (doc, index, docs) {
                let el = document.getElementById(doc.id.toString())
                if (el === null) { return }
                el.tabIndex = (index + 1).toString()
                if (index === (docs.length - 1)) {
                    el.focus()
                } else {
                    el.blur()
                }
            }
        )
    }

    parsedDocs () {
        return this.docs.map(
            function (doc) {
                return {
                    tokens: makeTokens(doc.text)
                    , id: doc.id
                }
            }
        )
    }
    tokens () {
        return this.parsedDocs.map(
            function (parsedDoc) { return parsedDoc.tokens || [] }
        )
    }
    dictionary () {
        return this.tokens.reduce(
            function (acc, tokens) {
                return acc.concat(tokens)
            }
            , []
        ).reduce(
            function (acc, word) {
                if (acc.indexOf(word) === -1) {
                    acc.push(word)
                    return acc
                } else {
                    return acc
                }
            }
            , []
        ).sort()
    }
    numberOfDocs () {
        return this.countVectors.reduce(
            function (acc, x, index) {
                return acc + (this.countVectors[index].length === 0 ? 0 : 1)
            }.bind(this)
            , 0
        )
    }
    countVectors () {
        return this.tokens.map(
            function (tokens) {
                return this.dictionary.map(
                    function (word) {
                        return tokens.reduce(
                            function (acc, token) { return token === word ? acc + 1 : acc }
                            , 0
                        )
                    }
                )
            }.bind(this)
        )
    }
    countVectorsT () {
        let arr = []
        this.countVectors.map(
            function (countVector, row, countVectors) {
                countVector.map(
                    function (count, col, countVector) {
                        if (row === 0) { arr.push([]) }
                        arr[col].push(count)
                    }
                )
            }
        )
        return arr
    }
    tfVectors () {
        return this.countVectors.map(
            function (countVector) {
                return makeTfVector(countVector)
            }
        )
    }
    idfVectors () {
        let total = this.numberOfDocs
        if (total === 0) { return this.countVectors.map(function () { return [] }) }
        let idfVector = this.countVectors[0].map(
            function (count, col) {
                let inDocCount = this.countVectorsT[col].reduce(
                    function (acc, x) {
                        return acc + (x > 0 ? 1 : 0)
                    }
                    , 0
                )
                if (total === 0) { return 0 }
                if (inDocCount === 0) { return 0 }
                return Math.log(total / inDocCount)
            }.bind(this)
        )
        return this.countVectors.map(function () { return idfVector })
    }
    tfIdfVectors () {
        return this.tfVectors.map(
            function (tfVector, row) {
                return tfVector.map(
                    function (tf, col) {
                        return tf * this.idfVectors[row][col]
                    }.bind(this)
                )
            }.bind(this)
        )
    }
    docsVectors () {
        return this.countVectors.map(
            function (countVector, index) {
                return [countVector, this.tfVectors[index], this.idfVectors[index], this.tfIdfVectors[index]]
            }.bind(this)
        )
    }
    queryTokens () {
        return makeTokens(this.query)
    }
    queryCountVector () {
        return this.dictionary.map(
            function (word) {
                return this.queryTokens.reduce(
                    function (acc, token) { return token === word ? acc + 1 : acc }
                    , 0
                )
            }.bind(this)
        )
    }
    queryTfVector () {
        return makeTfVector(this.queryCountVector)
    }
    queryIdfVector () {
        return this.idfVectors[0]
    }
    queryTfIdfVector () {
        return this.queryTfVector.map(
            function (tf, index) {
                return tf * this.queryIdfVector[index]
            }.bind(this)
        )
    }
    cosineSimilarities () {
        let mag = function (vector) {
            return Math.sqrt(
                vector.reduce(
                    function (acc, el) {
                        return acc + (el * el)
                    }
                    , 0
                )
            )
        }
        let queryMag = mag(this.queryTfIdfVector)
        return this.tfIdfVectors.map(
            function (tfIdfVector) {
                let dot = tfIdfVector.reduce(
                    function (acc, tfIdf, index) {
                        return acc + (tfIdf * this.queryTfIdfVector[index])
                    }.bind(this)
                    , 0
                )
                let docMag = mag(tfIdfVector)
                let mags = queryMag * docMag
                return mags === 0 ? 0 : dot / mags
            }.bind(this)
        )
    }
    bm25Scores () {
        let meanDocLen = 0
        if (this.numberOfDocs > 0) {
            meanDocLen = sum(
                this.countVectors.map(
                    function (countVector) {
                        return sum(countVector)
                    }
                )
            ) / this.numberOfDocs
        }
        let k1 = 1.2
        let k2 = 100
        let b = 0.75
        return this.countVectors.map(
            function (countVector) {
                return this.queryTokens.reduce(
                    function (acc, queryToken) {
                        let dictionaryIndex = this.dictionary.indexOf(queryToken)

                        let K = meanDocLen === 0 ? 0 : k1 * ((1 - b) + (b * (sum(countVector) / meanDocLen)))

                        let r = 0
                        let R = 0

                        let qf = dictionaryIndex < 0 ? 0 : this.queryCountVector[dictionaryIndex]
                        let n = 0
                        if (dictionaryIndex >= 0) {
                            n = this.countVectorsT[dictionaryIndex].reduce(
                                function (acc, x) { return acc + (x > 0 ? 1 : 0) }
                                , 0
                            )
                        }
                        let N = this.numberOfDocs
                        let f = dictionaryIndex < 0 ? 0 : countVector[dictionaryIndex]

                        let ai = r + 0.5
                        let bi = R - r + 0.5
                        let ci = n - r + 0.5
                        let di = N - n - R + r + 0.5
                        let ei = bi === 0 ? 0 : ai / bi
                        let fi = di === 0 ? 0 : ci / di
                        let gi = fi === 0 ? 0 : ei / fi

                        let hi = (k1 + 1) * f
                        let ii = K + f
                        let ji = ii === 0 ? 0 : hi / ii

                        let ki = (k2 + 1) * qf
                        let li = k2 + qf
                        let mi = li === 0 ? 0 : ki / li

                        return acc + (Math.log(gi) * ji * mi)
                    }.bind(this)
                    , 0
                )
            }.bind(this)
        )
    }
    tfIdfVsmRankedDocs () {
        return this.rankScoredDocs(this.cosineSimilarities)
    }
    bm25RankedDocs () {
        return this.rankScoredDocs(this.bm25Scores)
    }
}