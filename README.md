# HeavenFinder

Projet IA - ENSIIE
JUIN 2019 

GRIMALDI Xavier
DE SOUSA Emilio

Création d'un site de recherches d'annonces immobilières améliorées par le biais d'Intelligence Artificielle.

Possibilité de:
- Visualiser sur une carte isochrone, toutes les villes atteignables en 'X' mn à partir d'une ville sélectionné à l'avance.
- Recherche des annonces immobilières de plusieurs sites en passant par l'API de Scrapping Immobilier : Scrap'Immo
- Système de recherche améliorée à partir des description des annonces immobilières au travers d'un TF-IDF, et de différents mécanismes de Natural Language processing.


## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Run your end-to-end tests
```
npm run test:e2e
```

### Run your unit tests
```
npm run test:unit
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
