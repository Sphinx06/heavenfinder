function sortResults (json, prop, asc) {
    return json.sort(function (a, b) {
        if (asc) {
            return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0)
        } else {
            return (b[prop] > a[prop]) ? 1 : ((b[prop] < a[prop]) ? -1 : 0)
        }
    })
}


function multiThreadLemmaAnnonces () {
    let annoncesHandled = []
    let nb = annonces.ads.length
    let firsthalf = Math.round(nb / 2)
    let secondhalf = Math.round(nb / 2 + 1)
    const pool = new Pool()
    pool
        .run(function ([i, a], done) {
            var config = {
                tagTypes: ['ver', 'nom', 'adj'],
                strictness: false,
                minimumLength: 3,
                debug: false
            }
            const NlpjsTFr = require('nlp-js-tools-french')
            return new Promise(resolve => {
                let documents = []
                a.forEach(annonce => {
                    // 1.
                    let loweredDesc = annonce.description.toLowerCase()
                    // 2.
                    var nlpToolsFr = new NlpjsTFr(loweredDesc, config)
                    // 3.
                    let tokensLemma = []
                    let lemma = nlpToolsFr.lemmatizer()
                    lemma.forEach(item => {
                        if (!tokensLemma.includes(item.lemma))
                            tokensLemma.push(item.lemma)
                    })
                    // 4.
                    documents.push({ "document": i, "tokensLemma": tokensLemma })
                    i++
                })
                done(documents)
            })
        })
    Promise.all([
        pool.send([0, annonces.ads.slice(0, 100)]).promise().then(function success (message) { annoncesHandled.push(message) }, function error (error) { }),
        pool.send([100, annonces.ads.slice(100, 200)]).promise().then(function success (message) { annoncesHandled.push(message) }, function error (error) { }),
        pool.send([200, annonces.ads.slice(200, 300)]).promise().then(function success (message) { annoncesHandled.push(message) }, function error (error) { }),
        pool.send([300, annonces.ads.slice(300, 350)]).promise().then(function success (message) { annoncesHandled.push(message) }, function error (error) { }),
        pool.send([350, annonces.ads.slice(350, 400)]).promise().then(function success (message) { annoncesHandled.push(message) }, function error (error) { }),
        pool.send([400, annonces.ads.slice(400, nb)]).promise().then(function success (message) { annoncesHandled = annoncesHandled.concat(message) }, function error (error) { }),
    ]).then(function allResolved () {
        console.log('Everything done! It\'s closing time...')
        console.log(annoncesHandled)
        pool.killAll()
    })
}

// ###########################################
// Chargement des ressources 
// ############################################
/*const annonces = {
    "ads": [
        {
            "_id": "5cf7d9a4f6cb8a778f428ae4",
            "origin": "Leboncoin",
            "adId": "1627530634",
            "publicationDate": "2019-06-05T17:01:02.000Z",
            "title": "GRAND STUDIO 36M2 EVRY CENTRE",
            "type": "sales",
            "category": "flat",
            "description": "GRAND STUDIO refait à neuf EVRY CENTRE\nSitué au 2ème étage avec ascenseur, je vous propose ce grand studio de 36m2 entièrement rénové  avec entrée et placards intégrés, grand séjour de 20m2, cuisine indépendante aménagée et équipée ! , belle loggia de 3,5m2, salle de bain et WC indépendant. Place de parking en sous-sol avec accès ascenseur (très important sur evry centre) .\nGardien présent et vivant dans l'immeuble. \n A 5 min à pied du centre commercial d'Evry 2 et de l'Université. Et à 2 min à pied de la gare RER Evry-Courcouronnes.\n\n IDEAL INVESTISSEUR (louable autour de 750e) OU PREMIER ACHAT !\n\n\nA propos de la copropriété :\nNombre de lots : 150 \nCharges prévisionnelles annuelles : 2100 Euro(s)\n\nUne visite s'impose ! N'hésitez pas à me contacter pour plus de renseignements et photos/vidéos.",
            "url": "https://leboncoin.fr/ventes_immobilieres/1627530634.htm",
            "price": 96000,
            "city": "Evry",
            "postalCode": "91000",
            "rooms": 1,
            "square": 36,
            "pictureUrl": "https://img3.leboncoin.fr/ad-small/d2f12a807f12a56088d7340fddcd0c6919b0d1f9.jpg"
        },
        {
            "_id": "5cf7d9a4f6cb8a778f428ae5",
            "origin": "Leboncoin",
            "adId": "1606376509",
            "publicationDate": "2019-06-05T16:31:01.000Z",
            "title": "Appartement 4 pièces 88 m²",
            "type": "sales",
            "category": "flat",
            "description": "Appartement Evry 4 pièce(s) 88.65 m2\n\nAPPARTEMENT 4 PIÈCES CENTRE VILLE EVRY- 5 MIN A PIED DU RER ET DU CENTRE COMMERCIAL AGORA - Découvrez cet appartement T4 d'une surface habitable de 88.65 m², dans la ville d'EVRY , parfait pour investisseurs ou grande familles.  Il est situé au 8e et dernière étage d'un immeuble avec 2 ascenseurs. Le bâtiment est équipé d'un interphone. Il y a aussi un gardien. Cet appartement est organisé comme suit : un séjour de 25 m², une cuisine de 11 m²  semi ouverte, et trois chambres (une de 9 m², une de 10 m² et une de 12 m²). En ce qui concerne les pièces plus pratiques, il est composé d'une salle de bain, de WC et d'une buanderie de 4 m². Il dispose également d'une entrée de 10 m² et de deux dressings (un de 2 m² et un de 2 m²).   Une terrasse de 15 m² offre à cet appartement sans vis-a-vis avec magnifique vue dégagée, et un balcon coté chambre sans vis a vis également.  Tout est prévu pour les véhicules : parmi les emplacements disponibles dans l'immeuble, une place de parking est réservée pour ce bien. Pour vous chauffer, l'appartement possède des radiateurs, il s'agit d'un chauffage collectif.  EVRY, commune dans laquelle se situe cet appartement en vente, est une commune très dynamique. C'est une ville qui compte un grand centre commercial et une université. Elle est desservie par trois gares et 65 stations de taxis.  Cet appartement est exclusivement à vendre par Laforêt en mandat Favoriz. Découvrez toutes les originalités de ce bien immobilier en prenant RDV avec votre agence laforet. Copropriété de 150 lots (Pas de procédure en cours).    Charges annuelles : 4200 euros.\nRéférence annonce : 2513\nLes honoraires sont à la charge du vendeur\n\nA propos de la copropriété :\nNombre de lots : 150\nCharges prévisionnelles annuelles : 4200 €",
            "url": "https://leboncoin.fr/ventes_immobilieres/1606376509.htm",
            "price": 175000,
            "city": "Evry",
            "postalCode": "91000",
            "rooms": 4,
            "square": 88,
            "pictureUrl": "https://img1.leboncoin.fr/ad-small/fe650a1720ce6e394a48e7c5d95b391a65c1f4dd.jpg"
        },
        {
            "_id": "5cf7d9a4f6cb8a778f428ae6",
            "origin": "Leboncoin",
            "adId": "1606895918",
            "publicationDate": "2019-06-05T16:25:43.000Z",
            "title": "Appartement 1 pièce 39 m²",
            "type": "sales",
            "category": "flat",
            "description": "Appartement Evry 1 pièce(s) 39 m2\n\nDécouvrez cet appartement de 39 m², dans la ville d'EVRY (91000). Il est composé d'un séjour de 18 m² et d'une cuisine. Il dispose aussi D'UN DRESSING ET D'UN JARDIN. La surface inhabituellement grande permet de repenser ce T1 en T2!  Il se situe dans un immeuble construit en 1973. Il est équipé d'un ascenseur. Un interphone, un digicode et un gardien assurent un accès sécurisé.  La commune dans laquelle se trouve cet appartement en vente, EVRY,  Elle est desservie par 65 stations de taxis et trois gares.  Cet appartement est exclusivement à vendre par Laforêt en mandat Favoriz. Découvrez toutes les originalités de ce bien immobilier en prenant RDV avec votre agence Laforêt EVRY. Copropriété de 80 lots (Pas de procédure en cours).    Charges annuelles : 1652 euros.\nRéférence annonce : 2480\nLes honoraires sont à la charge du vendeur\n\nA propos de la copropriété :\nNombre de lots : 80\nCharges prévisionnelles annuelles : 1652 €",
            "url": "https://leboncoin.fr/ventes_immobilieres/1606895918.htm",
            "price": 99900,
            "city": "Evry",
            "postalCode": "91000",
            "rooms": 1,
            "square": 39,
            "pictureUrl": "https://img3.leboncoin.fr/ad-small/8a85604eae1f3183efaeb8bf1a38807b1216df38.jpg"
        },
        {
            "_id": "5cf7d9a4f6cb8a778f428ae7",
            "origin": "Leboncoin",
            "adId": "1614599504",
            "publicationDate": "2019-06-05T13:29:35.000Z",
            "title": "Appartement Mousseau",
            "type": "sales",
            "category": "flat",
            "description": "Appartement situé dans la résidence Alphonse Daudet, une résidence saine avec son parc arboré et une aire de jeu pour les enfants.\n\nIl est situé au 4eme et dernier étage sans ascenseur, belle vue sur le parc ,aucun vis à vis.\nL'appartement est constitué de 3 pièces avec plusieurs rangements.\nDeux rangements style dressing.\nIl est équipé d'une grande salle d'eau neuve avec une douche à l'italienne .\nun wc suspendu neuf.\nune grande cuisine équipée neuve.\nUne chambre parentale de 14m2.\nEt une autre chambre de 10m2.\nUn double séjour 30m2 , avec possibilité de faire  une troisième chambre.\nAppartement refait à neuf: plomberie,électricité,fenêtres avec volet roulant électrique sans fil,sol et peinture neuf etc .\nÉquipé de la fibre prise eternet dans toutes les chambres et le salon.\nBalcon carrelé.\nUne place de parking.\nUne cave.\nA proximité : écoles ,collège,2 gare à 5min à pied,\nClinique,hôpital,A6,n104 et n7.\n\nLe montant des charge s'élève à 215 Euros par mois .\n\nAppartement coup de coeur vous n'avez plus qu'à posez vos valises.\ncontactez moi  pour plus d'informations si vous êtes intéressé curieux et agence s'abstenir  : 0630694238\nBien cordialement",
            "url": "https://leboncoin.fr/ventes_immobilieres/1614599504.htm",
            "price": 163000,
            "city": "Evry",
            "postalCode": "91000",
            "rooms": 4,
            "square": 80,
            "pictureUrl": "https://img0.leboncoin.fr/ad-small/8bc5b3d86be482eaed430cb37ffd35babbbfdb1a.jpg"
        },
        {
            "_id": "5cf7d9a4f6cb8a778f428b16",
            "origin": "Leboncoin",
            "adId": "1627479223",
            "publicationDate": "2019-06-05T13:07:54.000Z",
            "title": "Appartement 1 pièce 32 m²",
            "type": "rentals",
            "category": "flat",
            "description": "EVRY AGUADO\nAppartement 1 pièce de 31.71 m² comprenant une entrée avec placard aménagé, une salle de bains avec WC, un pièce principale de 21.18 m², une kitchenette ouverte et équipée. Double vitrage, sans vis-à-vis. Box privatif en sous-sol.\nA deux pas des commerces, des bus et à 10 min de la gare RER.\nDisponible à partir du 22 juillet\nLoyer : 595.00 euros charges comprises\nProvision mensuelle sur charges récuperable : 50.00 euros\nDépôt de garantie : 545.00 euros\nHonoraire charge locataire :\n- Constitution du dossier, visites, rédaction du contrat : 317.10 Euros\n- Etat des lieux : 95.13 Euros\nRéférence annonce : 144",
            "url": "https://leboncoin.fr/locations/1627479223.htm",
            "price": 595,
            "city": "Evry",
            "postalCode": "91000",
            "rooms": 1,
            "square": 32,
            "pictureUrl": "https://img3.leboncoin.fr/ad-small/595ce1af9fc3c8711c637013811031b69b5406a3.jpg"
        },
        {
            "_id": "5cf7d9a4f6cb8a778f428b17",
            "origin": "Leb oncoin",
            "adId": "1627383095",
            "publicationDate": "2019-06-05T12:50:18.000Z",
            "title": "Appartement 3 pièces 64 m²",
            "type": "rentals",
            "category": "flat",
            "description": "Appartement\n\nEXCLUSIVITÉ - Dans la ville d'EVRY (91000), venez découvrir cet appartement F3 de 64 m². Ce bien offre deux chambres, une cuisine aménagée ainsi qu'une salle de bain.\nLe chauffage de la résidence est individuel, alimenté au gaz. Pour vos véhicules, l'appartement est mis en location avec un garage. La sécurité du bâtiment est assurée par un digicode.\nCet appartement en location se situe dans la commune d'EVRY, commune très calme. Cette ville très tranquille arbore trois supermarchés et quelques commerces et restaurants, ainsi que des infrastructures sportives. Tous les types d'écoles (maternelle, élémentaire et secondaire) y sont implantés, parmi lesquels le Lycée Polyvalent Privé Notre Dame de Sion et le Lycée Professionnel Charles Baudelaire, Lycée des Métiers de la Coiffure et de l'Esthétique, des Services à la Personnes. Elle est desservie par trois gares.\nContactez notre équipe pour plus de renseignements sur ce bien immobilier à EVRY.\n.  Le bien est soumis au statut de la copropriété .\n Loyer de 787,00 euros par mois charges comprises dont 90,00 euros par mois de provision pour charges (soumis à la régularisation annuelle).\n Soit avec Assurance Habitation et assistance* ( 15.00 euros ) : 802,00 euros.\n Les honoraires charge locataire sont de 832,00 euros ( soit 13,00 euros/m² ) dont 192,00 euros pour état des lieux ( soit 3,00 euros/m² ).\n Vous pouvez consulter les barèmes d'honoraires à l'adresse suivante :\n ***\n *Service facultatif. Contribution annuelle attentat non comprise. Voir conditions en agence.\nRéférence annonce : GES02050042-530\nHonoraires à la charge du locataire : 832 € TTC dont 192 € pour l'état des lieux\nDépôt de garantie : 697 €",
            "url": "https://leboncoin.fr/locations/1627383095.htm",
            "price": 787,
            "city": "Evry",
            "postalCode": "91000",
            "rooms": 3,
            "square": 64
        },
        {
            "_id": "5cf7d9a4f6cb8a778f428b18",
            "origin": "Leboncoin",
            "adId": "1627376340",
            "publicationDate": "2019-06-05T11:30:44.000Z",
            "title": "Appartement 3 pièces 60 m²",
            "type": "rentals",
            "category": "flat",
            "description": "Appartement 3 pièces\n\nSANS HONORAIRES. **Soyez les premiers locataires de la Résidence Bras de Fer** - Situé au 2ème étage avec ascenseur cet appartement de 3 pièces de 60 m² comprend : une entrée, une pièce principale avec cuisine ouverte avec une loggia de plus de 6 m² (orientée sud), deux chambres dont une de plus de 14 m², une salle de bains avec wc, un wc séparé. Une place de parking en sous-sol complète ce bien. Une location incluant le chauffage et l'eau chaude dans les charges.Résidence calme, sécurisée et idéalement située. A visiter rapidement.A proximité immédiate des transports et des commerces, la résidence de part sa situation et ses prestations offre un cadre de vie agréable Evry est desservie par de nombreux axes routiers : A6, N7 et la Francilienne permettant de relier Paris. La gare RER D à proximité immédiate rejoint Paris en 40 minutes. A proximité de la résidence se trouvent de nombreux commerces ainsi que le centre commercial EVRY 2. La commune bénéficie de tous les services de proximité et de nombreuses infrastructures dédiées à la santé. Evry est une commune attractive de l'Essonne située à 26 kilomètres au Sud Est de Paris, elle bénéficie de nombreux équipements : sportifs, socio-culturels et associatifs. Le quartier du Bras de Fer est en pleine mutation notamment avec l'arrivée du TZEN 4. Date disponibilité : 05/06/2019\nRéférence annonce : SNI_0000_278880\nMontant des charges : 165 € / mois",
            "url": "https://leboncoin.fr/locations/1627376340.htm",
            "price": 855,
            "city": "Evry",
            "postalCode": "91000",
            "rooms": 3,
            "square": 60,
            "pictureUrl": "https://img5.leboncoin.fr/ad-small/5110ce2d5e701cada1b95333842b8a3a8537684d.jpg"
        },
        {
            "_id": "5cf7d9a4f6cb8a778f428b19",
            "origin": "Leboncoin",
            "adId": "1614222929",
            "publicationDate": "2019-06-05T10:12:06.000Z",
            "title": "T2 meublé 40 m² proximité Evry",
            "type": "rentals",
            "category": "flat",
            "description": "A 5km d'Evry, sur la commune d'Ormoy.\nProche toutes commodités.\nCourt séjour de quelques semaines ou mois possible.\n\nDans une résidence récente, bel appartement T2 en duplex de 40 m², meublé et lumineux, comprenant : \n\n- Une entrée sur séjour meublé de 20 m² environ avec canapé convertible, table basse, table et chaises pour les repas, bureau et chaises.\n- Une kitchenette équipée : micro-ondes, frigo, hotte, plaques de cuisson, évier, rangements.\n- un placard/penderie.\n- Une salle d'eau avec cabine de douche, vasque, rangements, WC et sèche-serviette.\n- Une chambre en mezzanine avec lit double, dressing et espace TV/bureau.\n- Un grand balcon avec mobilier de jardin.\n\nPlace de stationnement dans un parking sécurisé.\n\nAucun frais.\n\n-",
            "url": "https://leboncoin.fr/locations/1614222929.htm",
            "price": 750,
            "city": "Evry",
            "postalCode": "91000",
            "rooms": 2,
            "square": 40,
            "pictureUrl": "https://img3.leboncoin.fr/ad-small/9ee544aae89c46cff0c17ad1e3219a1cb37ad973.jpg"
        },
        {
            "_id": "5cf7d9a4f6cb8a778f428ae8",
            "origin": "Leboncoin",
            "adId": "1605734853",
            "publicationDate": "2019-06-05T09:49:06.000Z",
            "title": "Appartement 2 pièces 53 m²",
            "type": "sales",
            "category": "flat",
            "description": "Vente Appartement 2 pièces\n\nIAD France - Christelle BERNARD (06 98 79 44 41) vous propose : EVRY VILLAGE - Résidence agréable et bien entretenue, proche écoles, bus, et commerces à pieds - Venez découvrir ce F2 et vous serez séduit: une pièce à vivre lumineuse et sans vis-à-vis donnant sur un espace piéton, une GRANDE CHAMBRE avec un immense placard intégré, une cuisine spacieuse, une salle de bain avec fenêtre!!Les +: chaque pièce a accès à un grand balcon pour profiter de l'extérieur, pas de travaux nécessaires! Vous disposez d'une CAVE et d'un PARKING en Silo.Et en HONORAIRES ADAPTéS...Appelez-vite!!!\nLa présente annonce immobilière vise 1 lot principal situé dans une copropriété formant 100 lots au total ne faisant l'objet d'aucune procédure en cours et d'un montant de charges d'environ 198 € par mois (soit 2376 € annuel) déclaré par le vendeur.Honoraires d'agence à la charge du vendeur.Information d'affichage énergétique sur ce bien : DPE D indice 189 et GES E indice 46. La présente annonce immobilière a été rédigée sous la responsabilité éditoriale de Mlle Christelle BERNARD (ID 433), Agent Commercial mandataire en immobilier immatriculé au Registre Spécial des Agents Commerciaux (RSAC) du Tribunal de Commerce de EVRY sous le numéro 521 609 313.\nRéférence annonce : 642819\n\nA propos de la copropriété :\nPas de procédure en cours\nNombre de lots : 300\nCharges prévisionnelles annuelles : 2376 €",
            "url": "https://leboncoin.fr/ventes_immobilieres/1605734853.htm",
            "price": 110000,
            "city": "Evry",
            "postalCode": "91000",
            "rooms": 2,
            "square": 53,
            "pictureUrl": "https://img1.leboncoin.fr/ad-small/5c8e232e2b31d8da69564ec3e76ab02ebbb85db1.jpg"
        },
        {
            "_id": "5cf7d9a4f6cb8a778f428b1a",
            "origin": "Leboncoin",
            "adId": "1627292298",
            "publicationDate": "2019-06-05T09:39:53.000Z",
            "title": "studio meublé grand bourg",
            "type": "renta ls",
            "category": "flat",
            "description": "Studio meublé refait à neuf tout équipé RER D Grand bourg 10 min à pied (frigo, machine à laver, hotte, plaque) canapé clic clac +armoire.Salle de bain avec toilette.Parking avec place attitré.chauffage + eau chaude collective comprit dans les charges. 3 fois le montant demandé soit 1600 euro net en CDI avec garant identique.Garantie visale de préférence \n.. Merci de m'envoyer un message je vous rappellerai si votre profil correspond pour une visite, disponible dès le premier juin.550 euro CC",
            "url": "https://leboncoin.fr/locations/1627292298.htm",
            "price": 550,
            "city": "Evry",
            "postalCode": "91000",
            "rooms": 1,
            "square": 15,
            "pictureUrl": "https://img5.leboncoin.fr/ad-small/d87dd18aa25a89be2e91d3c60d0ab88ef608017c.jpg"
        },
        {
            "_id": "5cf7d9a4f6cb8a778f428ae9",
            "origin": "Leboncoin",
            "adId": "1627349989",
            "publicationDate": "2019-06-05T09:13:59.000Z",
            "title": "Appartement 5 pièces 73 m²",
            "type": "sales",
            "category": "flat",
            "description": "Appartement Evry 4 pièce(s) 73 m2\n\nJAMAIS VU SUR EVRY un appartement aussi bien refait comprenant, séjour donnant sur terrasse, cuisine équipée, 4 chambres avec placards intégrés, 2 WC,  2 salles d'eau, double vitrage PVC, stores électriques, électricité refaite, parking S/SOL + Box en S/SOL. COUP DE COEUR ASSURE.\n165500 euros Honoraires à la charge du vendeur.\nRéférence annonce : E0GPSK\n\nA propos de la copropriété :\nNombre de lots : 350\nCharges prévisionnelles annuelles : 3192 €",
            "url": "https://leboncoin.fr/ventes_immobilieres/1627349989.htm",
            "price": 165500,
            "city": "Evry",
            "postalCode": "91000",
            "rooms": 5,
            "square": 73,
            "pictureUrl": "https://img6.leboncoin.fr/ad-small/fd7db755c6655a2f920fba830bebd202f9fac3fb.jpg"
        },
        {
            "_id": "5cf7d9a4f6cb8a778f428b1b",
            "origin": "Leboncoin",
            "adId": "1627376303",
            "publicationDate": "2019-06-05T09:08:08.000Z",
            "title": "Appartement 3 pièces 57 m²",
            "type": "rentals",
            "category": "flat",
            "description": "Appartement 3 pièces\n\nSANS HONORAIRES. Soyez les premiers locataires de la Résidence Bras de Fer - Situé au 2ème étage avec ascenseur cet appartement de 3 pièces en duplex de 57 m² comprend : une entrée avec placard, un séjour avec cuisine ouverte non aménagée ni équipée, le tout donnant sur une terrasse (orientée ouest), une chambre avec placard, un bureau, une salle de douches, une salle de bains avec wc, un wc indépendant. Une place de parking en sous-sol complète ce bien. Une location incluant le chauffage et l'eau chaude dans les charges.A proximité immédiate des transports et des commerces, la résidence de part sa situation et ses prestations offre un cadre de vie agréable Evry est desservie par de nombreux axes routiers : A6, N7 et la Francilienne permettant de relier Paris. La gare RER D à proximité immédiate rejoint Paris en 40 minutes. A proximité de la résidence se trouvent de nombreux commerces ainsi que le centre commercial EVRY 2. La commune bénéficie de tous les services de proximité et de nombreuses infrastructures dédiées à la santé. Evry est une commune attractive de l'Essonne située à 26 kilomètres au Sud Est de Paris, elle bénéficie de nombreux équipements : sportifs, socio-culturels et associatifs. Le quartier du Bras de Fer est en pleine mutation notamment avec l'arrivée du TZEN 4. Date disponibilité : 05/06/2019\nRéférence annonce : SNI_0000_278884\nMontant des charges : 157 € / mois",
            "url": "https://leboncoin.fr/locations/1627376303.htm",
            "price": 827,
            "city": "Evry",
            "postalCode": "91000",
            "rooms": 3,
            "square": 57,
            "pictureUrl": "https://img5.leboncoin.fr/ad-small/228486c849458d16686470dd49d2ae792894d521.jpg"
        },
        {
            "_id": "5cf7d9a4f6cb8a778f428aea",
            "origin": "Leboncoin",
            "adId": "1598218939",
            "publicationDate": "2019-06-05T09:07:30.000Z",
            "title": "Appartement 4 pièces 81 m²",
            "type": "sales",
            "category": "flat",
            "description": "Appartement de 4 pièces\n\nMAGNIFIQUE VUE DEGAGEE SUR LA SEINE. sans vis à vis Vous ne pouvez trouver endroit plus calme !!A 5 mn à pied de la gare RER. Appartement de type F4  compr.: ent., placard, cuisine équipée, séjour dble (possibilité de 3 e chambre), dégt, sdb, wc, 2 chbres, s.d'eau, nbreux rangts. balcon, cave Vendu avec 2 places de parking, rare à ce prix.\nRéférence annonce : 211\nLes honoraires sont à la charge du vendeur\n\nA propos de la copropriété :\nNombre de lots : 100\nCharges prévisionnelles annuelles : 2880 €",
            "url": "https://leboncoin.fr/ventes_immobilieres/1598218939.htm",
            "price": 161415,
            "city": "Evry",
            "postalCode": "91000",
            "rooms": 4,
            "square": 81,
            "pictureUrl": "https://img7.leboncoin.fr/ad-small/7062dff842e19eb1a038e7c5b064d060a6269a4b.jpg"
        }
    ]
}*/
const natural = require('natural')
const NlpjsTFr = require('nlp-js-tools-french')
const annonces = require('../ressources/getAnnoncesEvryParis_page1.json')
const Pool = require('threads').Pool
const sw = require('stopword')
//const { PerformanceObserver, performance } = require('perf_hooks')

// Déclaration de l'instances
var TfIdf = natural.TfIdf
var tfidf = new TfIdf()
var config = {
    tagTypes: ['ver', 'nom', 'adj'],
    strictness: false,
    minimumLength: 3,
    debug: false
}
var NGrams = natural.NGrams
var words = require('../mystopwords').words
// #########################################
// ETAPES DE GESTION TF-IDF DES ANNONCES
// Iteration sur chaque annonce
// 1. Lower Description Annonce
// 2. Tokenisation description de l'annonce
// 3. Lemmatization description de l'annonce
// 4. Ajout de l'annonce aux documents
let i = 0
annonces.ads.forEach(annonce => {
    // 1.
    let loweredDesc = annonce.description.toLowerCase()
    // 2.
    var nlpToolsFr = new NlpjsTFr(loweredDesc, config)
    // 3.
    let tokensLemma = []
    let lemma = nlpToolsFr.lemmatizer()
    lemma.forEach(item => {
        if (!tokensLemma.includes(item.lemma))
            tokensLemma.push(item.lemma)
    })
    let lemmaStop = sw.removeStopwords(tokensLemma, words)
    let test = NGrams.bigrams(lemmaStop)
    let toRemoveinQuery = []
    test.forEach(asso => {
        lemmaStop.push(asso.join(" "))
        if(asso[0] == 'sans' || asso[0] == 'pas'){
            toRemoveinQuery.push(asso[1])
        }
    })

    let finalQuery = lemmaStop.filter(function (ele){
        return (!toRemoveinQuery.includes(ele) && ele !== "sans" && ele !== "pas" && ele !=="avec")
    })
    // 4.
    if(i ==35){
        console.log(finalQuery)
    }
    tfidf.addDocument(finalQuery)
    i++
})


// #########################################
// ETAPES DE GESTION DE LA REQUËTE
// 0) Définition de la query
// 1) Lower Query
// 2) Tokenisation Query
// 3) Filtrage des mots non souhaitées (redondant avec le filtrage de la recherche)
// 4) Lemmatization Query
// 5) Lancement du TFIDF
// 6) Tri des résultats

// Déclaration des variables
let result = []
let adsSorted = null
let filterToRemove = ['appartement', 'maison']


// 0) Définition de la query
let query = "Je veux un appartement sans ascenseur, mais avec un gardien."
// 1) Lower Query
let lowQuery = query.toLowerCase()
// 2) Tokenisation Query
var nlpToolsFr = new NlpjsTFr(lowQuery, config)
// 3) Filtrage des mots non souhaitées (redondant avec le filtrage de la recherche)
let queryLemma = []
// 4) Lemmatization Query
let lemma = nlpToolsFr.lemmatizer()
lemma.forEach(item => {
    if (!filterToRemove.includes(item.word))
        queryLemma.push(item.lemma)
})

let queryLemmaStop = sw.removeStopwords(queryLemma, words)
let test = NGrams.bigrams(queryLemmaStop)
let toRemoveinQuery = []
test.forEach(asso => {
    queryLemmaStop.push(asso.join(" "))
    if(asso[0] == 'sans' || asso[0] == 'pas'){
        toRemoveinQuery.push(asso[1])
    }
})
let finalQuery = queryLemmaStop.filter(function (ele){
    return (!toRemoveinQuery.includes(ele) && ele !== "sans" && ele !== "pas" && ele !=="avec")
})
console.log(finalQuery)
// 5) Lancement du TFIDF
tfidf.tfidfs(finalQuery, function (i, measure) {
    annonces.ads[i]["measure"] = measure
    console.log({ "id": i, "measure": measure })
    if(measure > 10){
        console.log(annonces.ads[i].description)
    }
})

// 6) Tri des résultats
adsSorted = sortResults(annonces.ads, "measure", false)

