import Vuex from "vuex"
import Vue from "vue"

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    totalTvCount: {} // The TV inventory
  },

  getters: {
    // Here we will create a getter
  },

  mutations: {
    removeTv (state, cities) {
      // For now we allow Jenny just to remove
      // one TV at a time.
      state.totalTvCount = cities
    }
  },

  actions: {
    removeTv (context, amount) {
      // For now we only remove a tv,
      // but this action can contain logic
      // so we could expand it in the future.
      // If we enough TVs, ask Jenny to remove it
      context.commit("removeTv", amount)
    }
    // Here we will create Larry
  }
})
