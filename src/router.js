import Vue from "vue"
import Router from "vue-router"
import home from "./views/Home.vue"
import travelTime from "./views/TravelTime.vue"
import houseFinder from "./views/HouseFinder.vue"
Vue.use(Router)

export default new Router({
  routes: [
    {
      path: "/",
      name: "home",
      component: home
    },
    {
      path: "/app",
      name: "travelTime",
      component: travelTime
    },
    {
      path: "/houseFinder",
      name: "houseFinder",
      component: houseFinder
    },    
  ]
})
